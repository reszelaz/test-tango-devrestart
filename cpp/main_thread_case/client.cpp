#include <tango.h>

extern omni_thread::key_t key_py_data;

using namespace std;

int main()
{
    Tango::PyData *py_data_ptr = new Tango::PyData();
    omni_thread::self()->set_value(key_py_data,py_data_ptr);
    omni_thread::self()->remove_value(key_py_data);
    delete py_data_ptr;
}
