import tango
from tango.server import Device

class MyDevice(Device):

    def init_device(self):
        super().init_device()
        self.set_state(tango.DevState.ON)
