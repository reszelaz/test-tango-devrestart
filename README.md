# Reproduce SIGSEGV when dserver's DevRestart() is executed from a thread

## Steps to reproduce

1. Register in Tango Database one `DeviceServer` DS with instance name `test`
   with:
   - 1 device of `PoolDevice` class, with the following name: `test/pooldevice/1`
   - 1 device of `MyDevice` class, with the following name: `test/mydevice/1`

    ```console
    tango_admin --add-server DeviceServer/test PoolDevice test/pooldevice/1
    tango_admin --add-server DeviceServer/test MyDevice test/mydevice/1    
    ```

## cppTango

2. Compile the cpp example:

   ```console
   cd cpp
   make -f Makefile.multi
   ```
3. Start `DeviceServer`: $HOME/DeviceServers/DeviceServer test
4. Start client: `python3 client.py`
## PyTango

2. Start `DeviceServer`: `python3 DeviceServer.py test`
3. Start client: `python3 client.py`

## Result

While running the client the server crashes witch `SIGSEGV`.

Backtrace from the cppTango execution:

```
(gdb) bt
#0  Tango::AutoPyLock::AutoPyLock (this=<optimized out>) at utils.cpp:3269
#1  0x00007f2e4bd01daa in Tango::DServer::restart (this=this@entry=0x558ab0b0cc60, d_name="test/mydevice/1") at dserver.cpp:939
#2  0x00007f2e4bd0c24b in Tango::DevRestartCmd::execute (this=<optimized out>, device=0x558ab0b0cc60, in_any=...) at dserverclass.cpp:104
#3  0x00007f2e4bce6a74 in Tango::DeviceClass::command_handler (this=0x558ab0b09e20, device=device@entry=0x558ab0b0cc60, command="DevRestart", in_any=...) at deviceclass.cpp:1197
#4  0x00007f2e4bc9bee8 in Tango::DeviceImpl::command_inout (this=0x558ab0b0cc60, in_cmd=0x7f2e36ffcad0 "DevRestart", in_any=...) at device.cpp:1545
#5  0x00007f2e4bcb9756 in Tango::Device_2Impl::command_inout_2 (this=0x558ab0b0cc60, in_cmd=<optimized out>, in_data=..., source=Tango::CACHE_DEV) at device_2.cpp:438
#6  0x00007f2e4bcd2c5a in Tango::Device_4Impl::command_inout_4 (this=0x558ab0b0cc60, in_cmd=0x7f2e36ffcad0 "DevRestart", in_data=..., source=Tango::CACHE_DEV, cl_id=...) at device_4.cpp:473
#7  0x00007f2e4be7f102 in _0RL_lcfn_6fe2f94a21a10053_a3000000 (cd=0x7f2e36ffc3e0, svnt=<optimized out>) at tangoSK.cpp:5196
#8  0x00007f2e4a77f1fe in omni::omniOrbPOA::dispatch(omniCallDescriptor&, omniLocalIdentity*) () from /usr/lib/libomniORB4.so.1
#9  0x00007f2e4a765289 in omniLocalIdentity::dispatch(omniCallDescriptor&) () from /usr/lib/libomniORB4.so.1
#10 0x00007f2e4a772c77 in omniObjRef::_invoke(omniCallDescriptor&, bool) () from /usr/lib/libomniORB4.so.1
#11 0x00007f2e4be89f81 in Tango::_objref_Device_4::command_inout_4 (this=this@entry=0x7f2e28001190, command=<optimized out>, argin=..., source=<optimized out>, cl_ident=...) at tangoSK.cpp:5223
#12 0x00007f2e4bb4dd97 in Tango::Connection::command_inout (this=0x7f2e36ffcb40, command="DevRestart", data_in=...) at devapi_base.cpp:1356
#13 0x0000558aaf8d7210 in Tango::Connection::command_inout (this=0x7f2e36ffcb40, cmd_name=0x558aaf8de95a "DevRestart", d_in=...) at /usr/include/tango/Connection.h:258
#14 0x0000558aaf8d7038 in PoolDevice_ns::_restart_device () at PoolDevice.cpp:237
#15 0x00007f2e4a49b64e in omni_thread_wrapper () from /usr/lib/libomnithread.so.3
#16 0x00007f2e49df34a4 in start_thread (arg=0x7f2e36ffd700) at pthread_create.c:456
#17 0x00007f2e49298d0f in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:97
```

Backtrace from the PyTango execution:

```
#0  Tango::AutoPyLock::AutoPyLock (this=<optimized out>) at utils.cpp:3269
#1  0x00007f2b1de0bdaa in Tango::DServer::restart (this=0x555d92bc4060, d_name="test/mydevice/1") at dserver.cpp:939
#2  0x00007f2b1e999784 in boost::python::detail::invoke<int, void (Tango::DServer::*)(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&), boost::python::arg_from_python<Tango::DServer&>, boost::python::arg_from_python<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&> > (ac0=..., tc=<synthetic pointer>..., f=
    @0x555d92a97b28: (void (Tango::DServer::*)(Tango::DServer * const, const std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > &)) 0x7f2b1de0b360 <Tango::DServer::restart(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >&)>) at /usr/include/boost/python/detail/invoke.hpp:94
#3  boost::python::detail::caller_arity<2u>::impl<void (Tango::DServer::*)(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&), boost::python::default_call_policies, boost::mpl::vector3<void, Tango::DServer&, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&> >::operator() (args_=<optimized out>, this=0x555d92a97b28) at /usr/include/boost/python/detail/caller.hpp:223
#4  boost::python::objects::caller_py_function_impl<boost::python::detail::caller<void (Tango::DServer::*)(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&), boost::python::default_call_policies, boost::mpl::vector3<void, Tango::DServer&, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const&> > >::operator() (this=0x555d92a97b20, args=<optimized out>, kw=<optimized out>)
    at /usr/include/boost/python/object/py_function.hpp:38
#5  0x00007f2b1d83e4ad in boost::python::objects::function::call(_object*, _object*) const () from /usr/lib/x86_64-linux-gnu/libboost_python-py35.so.1.62.0
#6  0x00007f2b1d83e6a8 in ?? () from /usr/lib/x86_64-linux-gnu/libboost_python-py35.so.1.62.0
#7  0x00007f2b1d84533b in boost::python::detail::exception_handler::operator()(boost::function0<void> const&) const () from /usr/lib/x86_64-linux-gnu/libboost_python-py35.so.1.62.0
#8  0x00007f2b1e88b713 in boost::python::detail::translate_exception<Tango::NotAllowed, void (*)(Tango::NotAllowed const&)>::operator() (this=<optimized out>, translate=0x7f2b1e884d70 <translate_not_allowed(Tango::NotAllowed const&)>, 
    f=..., handler=...) at /usr/include/boost/python/detail/translate_exception.hpp:48
#9  boost::_bi::list3<boost::arg<1>, boost::arg<2>, boost::_bi::value<void (*)(Tango::NotAllowed const&)> >::operator()<bool, boost::python::detail::translate_exception<Tango::NotAllowed, void (*)(Tango::NotAllowed const&)>, boost::_bi::rrlist2<boost::python::detail::exception_handler const&, boost::function0<void> const&> > (f=..., a=<synthetic pointer>..., this=<optimized out>) at /usr/include/boost/bind/bind.hpp:388
#10 boost::_bi::bind_t<bool, boost::python::detail::translate_exception<Tango::NotAllowed, void (*)(Tango::NotAllowed const&)>, boost::_bi::list3<boost::arg<1>, boost::arg<2>, boost::_bi::value<void (*)(Tango::NotAllowed const&)> > >::operator()<boost::python::detail::exception_handler const&, boost::function0<void> const&> (a2=..., a1=..., this=<optimized out>) at /usr/include/boost/bind/bind.hpp:1318
#11 boost::detail::function::function_obj_invoker2<boost::_bi::bind_t<bool, boost::python::detail::translate_exception<Tango::NotAllowed, void (*)(Tango::NotAllowed const&)>, boost::_bi::list3<boost::arg<1>, boost::arg<2>, boost::_bi::value<void (*)(Tango::NotAllowed const&)> > >, bool, boost::python::detail::exception_handler const&, boost::function0<void> const&>::invoke (function_obj_ptr=..., a0=..., a1=...) at /usr/include/boost/function/function_template.hpp:138
#12 0x00007f2b1d84530a in boost::python::detail::exception_handler::operator()(boost::function0<void> const&) const () from /usr/lib/x86_64-linux-gnu/libboost_python-py35.so.1.62.0
#13 0x00007f2b1e88b6c3 in boost::python::detail::translate_exception<Tango::DeviceUnlocked, void (*)(Tango::DeviceUnlocked const&)>::operator() (this=<optimized out>, 
    translate=0x7f2b1e884cf0 <translate_device_unlocked(Tango::DeviceUnlocked const&)>, f=..., handler=...) at /usr/include/boost/python/detail/translate_exception.hpp:48
#14 boost::_bi::list3<boost::arg<1>, boost::arg<2>, boost::_bi::value<void (*)(Tango::DeviceUnlocked const&)> >::operator()<bool, boost::python::detail::translate_exception<Tango::DeviceUnlocked, void (*)(Tango::DeviceUnlocked const&)>, boost::_bi::rrlist2<boost::python::detail::exception_handler const&, boost::function0<void> const&> > (f=..., a=<synthetic pointer>..., this=<optimized out>) at /usr/include/boost/bind/bind.hpp:388
#15 boost::_bi::bind_t<bool, boost::python::detail::translate_exception<Tango::DeviceUnlocked, void (*)(Tango::DeviceUnlocked const&)>, boost::_bi::list3<boost::arg<1>, boost::arg<2>, boost::_bi::value<void (*)(Tango::DeviceUnlocked const&)> > >::operator()<boost::python::detail::exception_handler const&, boost::function0<void> const&> (a2=..., a1=..., this=<optimized out>) at /usr/include/boost/bind/bind.hpp:1318
[...]
```