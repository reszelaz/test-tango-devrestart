import threading

import tango
from tango.server import Device, command


class PoolDevice(Device):

    def init_device(self):
        super().init_device()
        self.set_state(tango.DevState.ON)
        
    def _restart_device(self, name):
        with tango.EnsureOmniThread():    
            # self.adm_device.DevRestart(name)
            self.adm_device.restart(name)
    
    @command(dtype_in=str)
    def RestartDevice(self, argin):
        # self.adm_device = tango.DeviceProxy("dserver/deviceserver/test")
        self.adm_device = tango.Util.instance().get_dserver_device()
        name = argin
        thread = threading.Thread(target=self._restart_device, args=(name, ))
        thread.start()
        thread.join()


        
 